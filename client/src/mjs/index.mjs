import * as hb from 'handlebars';
import * as helpers from "./helpers.mjs";

const renderOptions = {
    allowProtoPropertiesByDefault: true,
    allowProtoMethodsByDefault: true,
};

export function render(name, data) {
	const template = hb.partials[name];
    if (!template) {
    	console.warn(`Template not found: ${name}`);
        return `["${name}" missing]`;
    }
    const html = template(data, renderOptions);
    return html;
}

export function compile(strVal) {
    return hb.compile(strVal);
}