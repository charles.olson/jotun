const Jotun = {};

const renderOptions = {
    allowProtoPropertiesByDefault: true,
    allowProtoMethodsByDefault: true,
};

/// Wrapper to render template partials
Jotun.render = function(name, data) {
    const template = Handlebars.partials[name];
    if (!template) {
    	console.warn(`Template not found: ${name}`);
        return `["${name}" missing]`;
    }
    const html = template(data, renderOptions);
    return html;
};

Jotun.compile = function compile(strVal) {
    return Handlebars.compile(strVal);
}