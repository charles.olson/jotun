import * as util from 'util';
import * as fs from 'fs';
import * as pathUtil from 'path';

import concat from 'concat';
import { glob, globSync } from 'glob';

import Handlebars from 'handlebars';

import { Builder } from './builder.mjs';

export class TemplateBuilder extends Builder {
    constructor(config) {
        super(config);
    }

    onChange(path, stats) {
        if (!fs.lstatSync(path).isDirectory()) {
            const srcInfo = pathUtil.parse(path);

            // Create template names from relative directory names separated by underscores
            const relDir = pathUtil.relative(this.config.src, srcInfo.dir);
            const relPathList = relDir.split('/');
            relPathList.push(srcInfo.name);

            const templateName = relPathList.join('_');

            // Compile the handlebars template
            const templateStr = fs.readFileSync(path).toString('utf8');
            const templateJs = Handlebars.precompile(templateStr);

            if (templateJs) {
                // Create an intermediate file (to all be concatenated together later)
                const template = `Handlebars.partials['${templateName}']=template(${templateJs});`;
                const intPath = pathUtil.resolve(this.config.int, `${templateName}.js`);
                fs.writeFileSync(intPath, template);
                console.log(`wrote: ${intPath}`);
            }
        }

        // Create the merged template file from all the fragments (if the directory has already been completely scanned)
        if (this.isScanComplete) {
            this.mergeIntermediate();
        }
    }

    onInit() {
        // Triggered after all files have been scanned (intermediate directory should be ready)
        this.mergeIntermediate();
    }

    mergeIntermediate() {
        console.log('TemplateBuilder::mergeIntermediate:', this.config);
        const intGlobPath = pathUtil.resolve(this.config.int, '**/*');
        const globFiles = globSync(intGlobPath, {'follow': false});
        const destPath = pathUtil.resolve(this.config.dest, this.config.output);

        // Not sure if there's a synced concat
        concat(globFiles).then(blobJs => {
            const hack = "var template=Handlebars.template, templates=Handlebars.templates=Handlebars.templates || {};\n";
            blobJs = hack + blobJs;
            fs.writeFileSync(destPath, blobJs);

            const msg = `wrote: ${destPath}`;
            this.notify(msg, 'success');
        });
    }
};
TemplateBuilder.defaultConfig = {
    'src': 'template',
    'dest': '',
    'output': 'templates.js',
}
Builder.registerType('template', TemplateBuilder);