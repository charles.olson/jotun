import * as chokidar from 'chokidar';
import * as fs from 'fs';
import * as pathUtil from 'path';

import { strict as assert } from 'assert';

export class Builder {
    constructor(config) {
        this.config = config;

        // Remove intermediate files (dangerous; don't do this automatically)
        //this.deleteDirIfExist(this.config.int);
    }

    start() {
        console.log(`Builder::start persistent:${this.config.persistent}`);

        if (this.config.src === undefined) {
            console.error('Builder: src undefined');
            return;
        }
        if (this.config.dest === undefined) {
            console.error('Builder: dest undefined');
            return;
        }
        if (this.config.src === this.config.dest) {
            console.error('Builder: src and dest cannot be the same');
            return;
        }

        // Create intermediate and dest folders
        this.createDirIfNotExist(this.config.int);
        this.createDirIfNotExist(this.config.dest);

        // Initial file scan has completed
        this.isScanComplete = false;

        this.watcher = chokidar.watch(this.config.src, {
            ignored: /^\./,
            persistent: this.config.persistent,
            awaitWriteFinish: true,
        });

        const self = this;
        this.watcher
            .on('add', function(path, stats) { self._onChange(path, stats); })
            .on('addDir', function(path, stats) { self._onChange(path, stats); })
            .on('change', function(path, stats) { self._onChange(path, stats); })
            .on('unlink', function(path, stats) { self._onChange(path, stats); })
            .on('unlinkDir', function(path, stats) { self._onChange(path, stats); })
            .on('ready', function(path, stats) {
                // Triggered after first scan of all files
                self.isScanComplete = true;
                self._onInit();
            })
            .on('error', function(error) {
                self.notify('generic error', 'failure');
                console.log(error);
            });

        return this;
    }

    stop() {
        console.log('Builder::stop');
        if (this.watcher) {
            this.watcher.unwatch(this.config.src);
            this.watcher.close();
            this.watcher = undefined;
        }

        // Remove intermediate files
        this.deleteDirIfExist(this.config.int);
    }

    createDirIfNotExist(dir) {
        if (dir) {
            console.log('mkdir if not exists:', dir);
            if (!fs.existsSync(dir)) {
                fs.mkdirSync(
                    dir,
                    {recursive: true},
                    error => console.error(error)
                );
            }
        }
    }

    deleteDirIfExist(dir) {
        if (dir) {
            console.log('rm if exists:', dir);
            if (fs.existsSync(dir)) {
                console.log('rm -r', dir);
                fs.rmSync(
                    dir,
                    {recursive: true},
                    error => console.error(error)
                );
            }
        }
    }

    notify(msg, sound) {
        var title = 'Jotun';
        if (this.type) {
            title += ` : ${this.type}`;
        }
        if (this.config.output) {
            title += ` : ${this.config.output}`;
        }

        console.log(title, msg);
    }

    _onInit() {
        try {
            this.onInit();
        }
        catch(e) {
            this.notify('Error', 'failure');
            console.log('\nBuilder init error:');
            console.error(e);
        }
    }

    _onChange(path, stats) {
        try {
            this.onChange(path, stats);
        }
        catch(e) {
            this.notify('Error', 'failure');
            console.log('\nBuilder change error:');
            console.error(e);
        }
    }

    static registerType(typeStr, builderClass) {
        console.log(`Builder::registerType: ${typeStr}`);
        assert(!Builder.typeMap[typeStr], `Builder::registerType: '${typeStr}' already registered`);
        Builder.typeMap[typeStr] = builderClass;
    }

    static create(def, project) {
        console.log(`Builder::create:`, def);

        const builderClass = Builder.typeMap[def.type];
        assert(builderClass, `Builder::create: '${def.type}' not registered`);

        if (def.config === undefined) def.config = {};

        const builder = new builderClass(def.config, def.isRoot);
        builder.project = project;

        return builder;
    }

    static updateDefaults(def) {
        const builderClass = Builder.typeMap[def.type];

        if (def.config === undefined) {
            def.config = {};
        }

        // Layer the config onto the defaults
        if (builderClass.defaultConfig) {
            const baseConfig = Object.assign({}, builderClass.defaultConfig);
            def.config = Object.assign(baseConfig, def.config);
        }

        // Intermediate target should be same as source, if not specified
        if (def.config.int === undefined) {
            def.config.int = def.config.src;
        }
    }

    //
    // Pure Virtual
    //
    onInit() { }
    onChange(path, stats) { }
}
Builder.typeMap = {};