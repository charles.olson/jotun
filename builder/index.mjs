#!/usr/bin/env node

import path from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

import * as dotenv from 'dotenv';
const envPath = path.resolve(__dirname, '../.env');
dotenv.config({path: envPath});

import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';

import { Builder } from './builder.mjs';
import { ProjectBuilder } from './projectBuilder.mjs';
import { CssBuilder } from './cssBuilder.mjs';
import { TemplateBuilder } from './templateBuilder.mjs';
import { RawBuilder } from './rawBuilder.mjs';
import { JsBundleBuilder } from './jsBundleBuilder.mjs';
import { MjsBundleBuilder } from './mjsBundleBuilder.mjs';

// Development hack when running in locally-installed /app
if (!process.env.NODE_MODULES.endsWith('/node_modules')) {
    process.env.NODE_MODULES += '/node_modules';
}

global.clientDistPath = path.resolve(__dirname, '../client/dist');

console.log('\n=== Starting Jotun Preprocessor ===\n');
console.log(`CWD: ${process.cwd()}`);
console.log(`DIR: ${__dirname}`);
console.log(`ENV: ${envPath}`);
console.log(`NODE_MODULES: ${process.env.NODE_MODULES}`);
console.log(`CLIENT DIST PATH: ${clientDistPath}`);

const argv = yargs(hideBin(process.argv))
    .option('m', {
            'alias':    'monitor',
            'type':     'boolean',
            'default':  false,
            'describe': 'Continue monitoring files for changes',
    })
    .option('d', {
            'alias':    'dest',
            'nargs':    1,
            'default':  './dist',
            'describe': 'Root destination directory for output files',
    })
    .option('i', {
            'alias':    'int',
            'nargs':    1,
            'default':  './intermediate',
            'describe': 'Root directory for transient/intermediate output files',
    })
    .option('s', {
            'alias':    'src',
            'nargs':    1,
            'default':  './src',
            'describe': 'Root directory for source files',
    })
    .option('p', {
            'alias':    'project',
            'nargs':    1,
            'default':  'project.json',
            'describe': 'Project file',
    })
    .demandOption(['d'])
    .argv;

const pb = Builder.create({
    'type': 'project',
    'config': {
        'project': argv.project,
        'src': argv.src,
        'int': argv.int,
        'dest': argv.dest,
        'persistent': argv.monitor, // arg flag whether or not to keep monitoring files
        'isRoot': true,
    },
}).start();
