import * as pathUtil from 'path';
import * as fs from 'fs';

import { Builder } from './builder.mjs';


function removeComments(str) {
    str = str.replace(/\/\*[\s\S]*?\*\/|\/\/.*/g, '');
    str = str.replaceAll('${NODE_MODULES}', process.env.NODE_MODULES);
    return str;
}

const monitorTypeMap = {};

export class ProjectBuilder extends Builder {
    constructor(config) {
        super(config);

        this.filename = pathUtil.resolve(this.config.src, this.config.project);

        this.childList = [];
        this.loadProjectFile();
    }

    onInit() {
        //console.log(`ProjectBuilder::onInit`);
    }

    onChange(path, stats) {
        if (this.isScanComplete) {
            console.log('File in project changed: ', path);

            // Only update if it's the project file itself that has changed
            if (path !== this.filename) {
                return;
            }

            console.log('');
            console.log(`===`);
            console.log(`ProjectBuilder::onChange: reload`);
            console.log(`===`);
            // Shut down existing package builders and reset child list
            this.stop();
            this.loadProjectFile();
            this.start();
        }
    }

    loadProjectFile() {
        console.log(`ProjectBuilder::loadProjectFile: ${this.filename}`);

        // Load the project file
        const projectStr = removeComments(fs.readFileSync(this.filename).toString('utf8'));
        const project = JSON.parse(projectStr);

        console.log(project);

        if (!project.packages) {
            console.warn('No packages in project');
            project.packages = [];
        }

        if (!project.omitClient && this.config.isRoot) {
            console.log(`Copy client JS from ${clientDistPath} to ${this.config.dest}`);
            const clientBuilder = Builder.create({
                'type': 'raw',
                'config': {
                    'src': clientDistPath, // global
                    'int': null,
                    'dest': pathUtil.resolve(this.config.dest, 'jotun/'),
                    'persistent': false,
                }
            });
            this.childList.push(clientBuilder);
        }

        // Create children
        for (const def of project.packages) {
            // Fill in missing values in the def
            Builder.updateDefaults(def);

            // Append relative paths
            this.updateRelativePath(def.config, 'src');
            this.updateRelativePath(def.config, 'int');
            this.updateRelativePath(def.config, 'dest');

            // Inherit the persistence of the project, unless explicitly disabled
            if (def.config.persistent !== false) {
                def.config.persistent = this.config.persistent;
            }

            const child = Builder.create(def, this);

            this.childList.push(child);
        }
    }

    updateRelativePath(childConfig, key) {
        if (childConfig[key]) {
            childConfig[key] = pathUtil.resolve(this.config[key], childConfig[key]);
        }
        else {
            childConfig[key] = pathUtil.resolve('./', this.config[key]);
        }
    }

    stop() {
        this.stopChildren();
        super.stop();
    }

    start() {
        super.start();
        this.startChildren();
    }

    stopChildren() {
        console.log('ProjectBuilder::resetChildren');
        if (this.childList) {
            for (const child of this.childList) {
                child.stop();
            }
        }
        this.childList = [];
    }

    startChildren() {
        console.log('ProjectBuilder::startChildren');
        if (this.childList) {
            for (const child of this.childList) {
                child.start();
            }
        }
    }
}
ProjectBuilder.defaultConfig = {
    'src': 'src',
    'int': 'int',
    'dest': 'dist',
    'project': 'project.json',
};
Builder.registerType('project', ProjectBuilder);