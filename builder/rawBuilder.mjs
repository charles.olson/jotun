import * as util from 'util';
import * as fs from 'fs';
import * as pathUtil from 'path';

import { Builder } from './builder.mjs';


export class RawBuilder extends Builder {
    onChange(path, stats) {
        // Copy each element to the dest, any time it is initialized or modified
        // (don't care if scan is complete)
        if (!fs.lstatSync(path).isDirectory()) {
            // Copy the file to the same relative path in the destination folder
            const relPath = pathUtil.relative(this.config.src, path);
            const destPath = pathUtil.join(this.config.dest, relPath);
            const destDir = pathUtil.dirname(destPath);

            // Create the destination directory if it doesn't already exist
            fs.mkdirSync(destDir, { recursive: true });

            fs.copyFileSync(path, destPath);
            //console.log(`RawMonitor: copied: ${path} => ${destPath}`);
        }
    }

    onInit() {
        // Do nothing
    }
};
RawBuilder.defaultConfig = {
    'src': 'asset',
    'dest': 'asset',
    'int': null,
};
Builder.registerType('raw', RawBuilder);