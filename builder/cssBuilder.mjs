import * as util from 'util';
import * as fs from 'fs';
import * as pathUtil from 'path';

import concat from 'concat';

import { minify } from 'terser';
import sass from 'sass';

import { Builder } from './builder.mjs';


export class CssBuilder extends Builder {
    onChange(path, stats) {
        if (this.isScanComplete) {
            console.log('\nCSS change:', path);
            if (!this.config.files || !this.config.files.length) {
                return;
            }

            // Always re-compile all files in this project if any file in the host directory changed
            //  because we don't specify included files of scss and don't necessarily monitor them

            // Create an intermediate file that points to the sources
            //  so SCSS can generate an accurate map file
            //  (rather than blobbing everything together)
            const intPath = pathUtil.resolve(this.config.int, `${this.config.output}.scss`);
            const destFilePath = pathUtil.resolve(this.config.dest, this.config.output);
            const destMapPath = `${destFilePath}.map`;

            console.log('  generating intermediate SCSS:', intPath);

            var intScssContents = '';
            for (const rootFileName of this.config.files) {
                const rootFilePath = pathUtil.resolve(this.config.src, rootFileName);
                const relativeFilePath = pathUtil.relative(this.config.int, rootFilePath);
                console.log(' import:', relativeFilePath);
                intScssContents += `@import "${relativeFilePath}";\n`;
            }
            // Write the intermediate SCSS
            fs.writeFileSync(intPath, intScssContents);

            console.log('  writing final CSS:', destFilePath, destMapPath);

            // Compile the intermediate SCSS
            // const result = sass.renderSync({
            //     'file': intPath,
            //     'outFile': destFilePath,
            //     'outputStyle': this._def.uncompressed ? 'expanded' : 'compressed',
            //     'sourceMap': destMapPath,
            //     'sourceMapEmbed': true,
            // });

            const options = {
                sync: 'sync',
                sourceMap: true,
                style: this.config.uncompressed ? 'expanded' : 'compressed',
            };
            const result = sass.compile(intPath, options);
            const finalCss = result.css + `/*# sourceMappingURL=${this.config.output}.map */`;
            const finalMap = JSON.stringify(result.sourceMap);

            fs.writeFileSync(destFilePath, finalCss);
            fs.writeFileSync(destMapPath, finalMap);

            const msg = `wrote: ${destFilePath}`;
            this.notify(msg, 'success');
        }
    }

    onInit() {
        this.onChange(this.config.src);
    }
};
CssBuilder.defaultConfig = {
    'src': 'css',
    'dest': '',
    'int': 'css',
    'output': 'styles.css',
};
Builder.registerType('css', CssBuilder);