import assert from 'assert/strict';

import * as fs from 'fs';
import * as pathUtil from 'path';

import concat from 'concat';
import glob from 'glob';
import { minify } from 'terser';

import { rollup } from 'rollup';
import commonjs from '@rollup/plugin-commonjs';
import { nodeResolve } from '@rollup/plugin-node-resolve';
import terser from '@rollup/plugin-terser';
import globals from 'rollup-plugin-node-globals';
import builtins from 'rollup-plugin-node-builtins';

import { Builder } from './builder.mjs';


export class MjsBundleBuilder extends Builder {
    onInit() {
        this.onChange(this.config.src);
    }

    onChange(path, stats) {
        const self = this;

        if (!this.isScanComplete) {
            // No intermediate generation until the directory has been fully scanned
            // (then the initial build gets triggered from OnInit)
            return;
        }

        const entryFile = pathUtil.resolve(this.config.src, this.config.index);
        const relSrc = pathUtil.relative(this.config.dest, this.config.src);
        const namespace = this.config.namespace ? this.config.namespace : pathUtil.basename(this.config.index, '.mjs');
        console.log({
            'entryFile': entryFile,
            'relSrc': relSrc,
            'namespace': namespace,
        });

        const localModulesPath = pathUtil.resolve(this.project.config.src, '../node_modules');
        const modulePaths = [
            process.env.NODE_MODULES,
            localModulesPath
        ];

        const inputOptions = {
            input: entryFile,
            external: [],
            plugins: [
                nodeResolve({
                    browser: true,
                    modulePaths: modulePaths,
                }),
                commonjs(),
                terser({
                    warnings: true,
                    nameCache: MjsBundleBuilder.nameCache,
                    // Use inline sourceMap instead:
                    // sourceMap: {
                    //     filename: this.config.output,
                    //     root: relSrc,
                    //     url: `${this.config.output}.map`,
                    // },
                }),
                globals(),
                builtins()
            ]
        };
        const outputOptions = {
            format: 'iife',
            name: namespace,
            sourcemap: 'inline', // true
        };

        this.build(inputOptions, outputOptions);
    }

    async build(inputOptions, outputOptions) {
        const startTimeMS = Date.now();
        console.log(`\nMJS build: ${this.config.index}`);

        let bundle;
        try {
            // create a bundle
            bundle = await rollup(inputOptions);

            // an array of file names this bundle depends on
            console.log(bundle.watchFiles);
        }
        catch (error) {
            console.error(error);
            return;
        }

        try {
            const {output} = await bundle.generate(outputOptions);
            await bundle.close();

            const result = output[0];

            const outputPath = pathUtil.resolve(this.config.dest, this.config.output);
            const outputMapPath = `${outputPath}.map`;

            fs.writeFileSync(outputPath, result.code);
            // Inline
            //fs.writeFileSync(outputMapPath, result.map);
        }
        catch (error) {
            console.error(error);
        }

        const elapsedTimeMS = Date.now() - startTimeMS;
        const elapsedTimeS = (0.001 * elapsedTimeMS).toFixed(2);
        console.log(` took ${elapsedTimeS} seconds`);
    }
};
MjsBundleBuilder.defaultConfig = {
    'src': 'mjs',
    'dest': '',
    'int': null,
    'index': 'index.mjs',
    'output': 'output.js',
    'namespace': null
};
MjsBundleBuilder.nameCache = {};

Builder.registerType('mjs', MjsBundleBuilder);