import assert from 'assert/strict';

import * as fs from 'fs';
import * as pathUtil from 'path';

import concat from 'concat';
import glob from 'glob';
import { minify } from 'terser';

import { rollup } from 'rollup';
import commonjs from '@rollup/plugin-commonjs';
import { nodeResolve } from '@rollup/plugin-node-resolve';

import { Builder } from './builder.mjs';


export class JsBundleBuilder extends Builder {
    onChange(path, stats) {
        const self = this;

        if (!this.isScanComplete) {
            // No intermediate generation until the directory has been fully scanned
            // (then the initial build gets triggered from OnInit)
            return;
        }

        const startTimeMS = Date.now();
        console.log(`\nJS change: ${path}`);
        
        // Check if the modified file belongs to this package (unless it's initing the entire path)
        if (!fs.lstatSync(path).isDirectory()) {
            const relPath = pathUtil.relative(this.config.src, path);
            if (pathUtil.extname(path) !== '.mjs') {
                if (!this.config.files || !this.config.files.includes(relPath)) {
                    console.log(`ignored '${path}'; not in package for '${this.config.output}'`);
                    return;
                }
            }
        }

        // Rebuild the package
        const outputPath = pathUtil.resolve(this.config.dest, this.config.output);
        const outputMapPath = `${outputPath}.map`;
        console.log('JsBuilder: build:', outputPath);

        (async() => {
            if (!this.config.files || !this.config.files.length) {
                return;
            }

            // Build dictionary of filenames and contents
            const fileContentsMap = {};
            for (const fileName of this.config.files) {
                const filePath = pathUtil.resolve(this.config.src, fileName);
                const fileExt = pathUtil.extname(fileName);
                if (fileExt === '.mjs') {
                    // Compile module JS using rollup
                    fileContentsMap[fileName] = await this.compile(filePath);
                }
                else {
                    // Straight common js to pass to terser/minifier
                    const fileContents = fs.readFileSync(filePath).toString('utf8');
                    fileContentsMap[fileName] = fileContents;
                }
            }

            // (src is only accessible from browser if the doc root is below the src folder on the web server)
            const relSrc = pathUtil.relative(this.config.dest, this.config.src);

            // Minify with Terser
            try {
                if (Object.keys(fileContentsMap).length) {
                    let result = await minify(fileContentsMap, {
                        warnings: true,
                        nameCache: JsBundleBuilder.nameCache,
                        sourceMap: {
                            filename: this.config.output,
                            root: relSrc,
                            url: `${this.config.output}.map`,
                        }
                    });

                    //console.log('RESULT', result);

                    fs.writeFileSync(outputPath, result.code);
                    fs.writeFileSync(outputMapPath, result.map);
                    console.log('JsBuilder: wrote:', outputPath, outputMapPath);

                    const msg = `wrote: ${outputPath}`;
                    self.notify(msg, 'success');
                }
            }
            catch(e) {
                console.error(e);
                self.notify('js error', 'failed');
            }
            finally {
                const elapsedTimeMS = Date.now() - startTimeMS;
                const elapsedTimeS = (0.001 * elapsedTimeMS).toFixed(2);
                console.log(` took ${elapsedTimeS} seconds`);
            }
        })();
    }

    onInit() {
        this.onChange(this.config.src);
    }

    async compile(entryFile) {
        const inputOptions = {
            input: entryFile,
            external: [],
            plugins: [
                nodeResolve({
                    browser: true,
                    modulePaths: [
                        process.env.NODE_MODULES,
                    ],
                }),
                commonjs(),
                //globals(),
                //builtins()
            ]
        }

        const namespace = this.config.namespace ? this.config.namespace : pathUtil.basename(entryFile, '.mjs');

        const outputOptions = {
            format: 'iife',
            name: namespace,
            sourcemap: 'inline',
        };

        try {
            const bundle = await rollup(inputOptions);
            const { output } = await bundle.generate(outputOptions);

            // Only deal with one output chunk
            assert.equal(output.length, 1);
            //console.log('  modules:', output[0].modules);

            return output[0].code;
        }
        catch(e) {
            console.error(e);
            this.notify('js module error', 'failed');
        }
    }
};
JsBundleBuilder.defaultConfig = {
    'src': 'js',
    'dest': '',
    'int': null,
    'output': 'output.js',
};
JsBundleBuilder.nameCache = {};

Builder.registerType('js', JsBundleBuilder);