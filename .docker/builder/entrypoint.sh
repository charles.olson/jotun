#!/bin/sh

echo "=== Builder ==="

echo "Starting Jotun Builder"
node /usr/node_modules/@keyrock-dev/jotun \
  --src /app/example/src \
  --int /app/example/int \
  --dest /app/example/dist \
  --project project-root.json \
  --monitor

status=$?
if [ $status -ne 0 ]; then
  echo "Builder failed: $status"
  #exit $status

  echo "\n=== RUN FOREVER (to debug) ==="
  echo "open dev console with:"
  echo "docker exec -it jotun_builder_1 /bin/bash"
  tail -f /dev/null
fi
