#!/bin/sh

# Start the server
echo "NGINX starting"
  echo "Test content at:"
  echo "http://localhost/"

nginx

status=$?
if [ $status -ne 0 ]; then
  echo "NGINX failed: $status"
  exit $status
fi
