#!/bin/sh

echo "=== Tools ==="

echo "\n== Test Build Client =="
./build-client

echo "\n== Test Build Example =="
./build-example

echo "\n=== RUN FOREVER ==="
echo "open dev console with:"
echo "docker exec -it jotun_tools_1 /bin/bash"
tail -f /dev/null
