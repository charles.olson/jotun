# jotun ("yoh-tun")
Web Content Pre-Processor


## Why?

Cornerstone JS Framework needs html templates (Handlebars/Jinja) compiled into js. Might as well handle mjs and SCSS at the same time.

Jotun allows packages (js, css, html, and assets) to be included in other projects, to simplify distribution of web component libraries.


## Development

Build-and-run the docker-compose file. No need to install npm locally (or install package dependencies) for development.

```
cd .docker
docker-compose build
docker-compose up
```


To iterate on the Builder itself (Node.js):

```
docker exec -it jotun_tools_1 /bin/bash
./build-client
./build-example
```


To iterate on the Example content:

browse to:
http://localhost


## Deploying Updates

To publish the latest package to NPM:

1. Increment the version number in package.json.
2. Commit and push to main.
