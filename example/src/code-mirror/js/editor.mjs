import {basicSetup} from "codemirror"
import {EditorView, keymap} from "@codemirror/view"
import {javascript} from "@codemirror/lang-javascript"

export function createEditor(target) {
    const editor = new EditorView({
        doc: `// Javascript editor`,
        extensions: [basicSetup, javascript()],
        parent: target,
    });
}
