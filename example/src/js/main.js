// Set the current theme
const initTheme = localStorage.getItem('theme');
if (initTheme) {
    document.querySelector('[data-option-key="theme"]').value = initTheme;
    document.documentElement.setAttribute(`data-theme`, initTheme);
}

function changeOption(el) {
	const key = el.dataset.optionKey;
    const val = el.value;
    console.log(`set option: ${key} = ${val}`);
    document.documentElement.setAttribute(`data-${key}`, val);
    localStorage.setItem(key, val);
}

// Render the example template
document.querySelector('#target').innerHTML = Jotun.render('widget_sample', {'test_param': 'TEST'});
document.querySelector('#bad-target').innerHTML = Jotun.render('should_be_missing');

// Create the CodeMirror editor
editor.createEditor(document.querySelector('#editor'));